<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RestModel extends CI_Model {

  public function getAlphabates(){
  	$response = array();
  	$this->db->select('*');
  	$this->db->where('name', 'en');
  	$sql = $this->db->get('alphabet');
  	$response = $sql->result_array();
  	return $response;
  }
}