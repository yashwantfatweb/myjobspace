import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home1 from './home/Home';
import Login from './login/Login';
import Company from './browsecompany/Company';
import Registration from './login/Registration';
import Header from './common/Header';
import Footer from './common/Footer';
import Jobs from './listing/Jobs';
class App extends Component {

  render() {
    return (
      <Router>
      <div>
         <Header />
          <Switch>
              <Route exact path='/' component={Home1} />
              <Route exact path='/login' component={Login} />
              <Route exact path='/browse-by-company' component={Company} />
              <Route exact path='/registration' component={Registration} />
              <Route path="/:category-jobs" component={Jobs} />
              <Route path="/jobs-in-:location" component={Jobs} />
              <Route path="/:category-jobs-in-:location" component={Jobs} />
          </Switch>
          <Footer />
          </div>
      </Router>
    );
  }
}

export default App;
