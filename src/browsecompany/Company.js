import React, {Component} from 'react';
import MetaTags from 'react-meta-tags';
export default class Company extends Component {
    constructor (props){
        super(props);
        this.state={
            item:[],
            isLoaded:false,
        }
    }
    componentDidMount(){
        fetch('/newapi/index.php/item/').then(res=>res.json()).then(json=>{
            this.setState({
                isLoaded:true,
                item:json,
            })
        });
    }
    render(){
        var {isLoaded,item} = this.state;
        if(!isLoaded){
            return <div>Loading...</div>;
        }
        else{
        return (
            <div>
                <MetaTags>
                    <title>Recruiting Staff, Advertising Jobs| Myjobspace NZ</title>
                    <meta id="meta-description" name="description" content="List you employment position on Myjobspace, NZ’s user-friendly, cost-effective job site for employers." />
                    <meta id="og-title" property="og:title" content="MyApp" />
                    <meta id="og-image" property="og:image" content="path/to/image.jpg" />
                 </MetaTags>
            	<div id="main-div-after" className="innerdiv_forall">
                    <h1>Search by Company</h1>
                    <div>
                        <div className="browseCompanyAB">
                            <span>
                                {item[0].value}
                            </span>
                        </div>
                        <div className="clr"></div>
                    </div>
                    <form action="https://www.myjobspace.co.nz/browse-by-company/" id="search_form">
                        <input type="hidden" name="action" value="search" />
                        <fieldset className="srh_by_company">
                            <div className="bcName">Company Name</div>
                            <div className="bcField"><input type="text" name="CompanyName" id="CompanyName" className="searchStringLike ac_input" value="" autoComplete="off" />
                            </div>
                        </fieldset>
                        <fieldset className="srh_by_company">
                            <div className="bcName">
                                Location
                            </div>
                            <div className="bcField">
                                <input type="text" id="Location" name="Location[location][value]" value="" className="location ac_input" autoComplete="off" />
                            </div>
                        </fieldset>
                        <fieldset className="srh_by_company">
                            <div className="bcName">&nbsp;</div>
                            <div className="bcField"><input type="submit" className="button" value="Find" /></div>
                        </fieldset>
                    </form>
                </div>
            </div>
            
        )
        }
    }
}
