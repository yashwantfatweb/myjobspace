import React, {Component} from 'react';

export default class Footer extends Component {
    render(){
        return (
			<div>
				<footer className="footer">
                    <div className="container">
                        <div>
                            <div>
                                <ul>
                                    <li><a title="Home" className="footer-nav__link" href="https://www.myjobspace.co.nz">Home</a></li>
                                    <li><a title="Contact" className="footer-nav__link" href="https://www.myjobspace.co.nz/contact/">Contact</a></li>
                                    <li><a title="About Us" className="footer-nav__link" href="https://www.myjobspace.co.nz/about/">About Us </a></li>
                                    <li><a title="Terms &amp; Conditions" className="footer-nav__link" href="https://www.myjobspace.co.nz/terms-of-use/">Terms & Conditions</a></li>
                                    
                                </ul>
                            </div>
                            <div>
                                <ul>
                                <li>Employer</li>
                                    <li><a title="Pricing" className="footer-nav__link" href="https://www.myjobspace.co.nz/employer-products/">Pricing</a></li>
                                    <li><a title="Register" className="footer-nav__link" href="https://www.myjobspace.co.nz/registration/?user_group_id=Employer">Register </a></li>
                                    <li><a title="Search CV" className="footer-nav__link" href="https://www.myjobspace.co.nz/search-resumes/">Search CV</a></li>
                                    <li><a title="testimonials" className="footer-nav__link" href="https://www.myjobspace.co.nz/pages/testimonials.php">Testimonials</a></li>
                                    <li><a title="Pricing" className="footer-nav__link" href="https://www.myjobspace.co.nz/terms-of-use/">Terms &amp; Conditions</a></li>
                                    <li><a title="Search Resumes" className="footer-nav__link" href="https://www.myjobspace.co.nz/blog/category/4/">Employment cases of interest</a></li>
                                  
                                </ul>

                            </div>
                            <div>
                                <ul>
                                    <li>Job Seeker</li>
                                    <li><a title="Find Jobs" className="footer-nav__link" href="https://www.myjobspace.co.nz/?user_group_id=JobSeeker">Register</a></li>
                                    <li><a title="Create Resume" className="footer-nav__link" href="https://www.myjobspace.co.nz/career/">Articles of interest</a></li>
                                    <li><a title="Sign in" className="footer-nav__link" href="https://www.myjobspace.co.nz/blog/">Blog</a></li>
                                    <li><a title="Sign in" className="footer-nav__link" href="https://www.myjobspace.co.nz/find-jobs/
                ">Advanced Job Search</a></li>
                                </ul>
                            </div>
                            <div id="social-media-wrapper">
                                <ul>
                                    <li><a title="Facebook" className="footer-nav__link footer-nav__link-social footer-nav__link-facebook" href="https://www.facebook.com/myjobspacenz/">Facebook</a></li>
                                    <li><a title="Twitter" className="footer-nav__link footer-nav__link-social footer-nav__link-twitter" href="https://twitter.com/myjobspace_nz/">Twitter</a></li>
                                    <li><a title="Google Plus" className="footer-nav__link footer-nav__link-social footer-nav__link-plus" href="https://plus.google.com/u/0/104902698381487930055">Google Plus</a></li>
                                    <li><a title="LinkedIn" className="footer-nav__link footer-nav__link-social footer-nav__link-in" href="https://www.linkedin.com/company/myjobspacenz/">LinkedIn</a></li>
                                    <li><a title="Instagram" className="footer-nav__link footer-nav__link-instagram" href="https://www.instagram.com/myjobspacenz/"><i className="fa fa-instagram" aria-hidden="true"></i>Instagram</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                        <div>Copyright © 2015-2019 MYJOBSPACE</div>
                    </div>
                </footer>
			</div>

        )
    }
}
