import React, {Component} from 'react';
import { Link } from 'react-router-dom';
export default class Header extends Component {
    render(){
        return (
    <div>
        <div id="loading"></div>
            <div id="messageBox"></div>
                <nav className="navbar navbar-default">
                    <div className="container container-fluid">
                        <div className="logo navbar-header">
                            <Link to="/" className="logo__text navbar-brand">
                                <img src="https://www.myjobspace.co.nz/templates/New/main/images/logo.png" border="0" alt="MyJobSpace NZ" title="MyJobSpace NZ" />
                            </Link>
                        </div>
                        <div className="burger-button__wrapper burger-button__wrapper__js visible-sm visible-xs" data-target="#navbar-collapse" data-toggle="collapse">
                            <div className="burger-button"></div>
                        </div>
                        <div className="collapse navbar-collapse" id="navbar-collapse">
                            <div className="visible-sm visible-xs">
                                <ul className="nav navbar-nav navbar-left">
                                    <li className="navbar__item"><a title="Jobs" className="navbar__link" href="https://www.myjobspace.co.nz">Home</a></li>
                                    <li className="navbar__item"><a title="Search a CV" className="navbar__link" href="https://www.myjobspace.co.nz/search-resumes/">Search</a></li>
                                    <li className="navbar__item"><Link to="/browse-by-company" title="Companies" className="navbar__link">Companies</Link></li>
                                    <li className="navbar__item dropdown"><a title="Employers" className="navbar__link dropdown-toggle" href="#"><i style={{fontSize:'20px',paddingRight: '5px'}} className="fa"></i>Employers</a>
                                        <ul className="dropdown-menu">
                                            <li><a href="https://www.myjobspace.co.nz/employer-products/">Pricing</a></li>
                                            <li><a href="https://www.myjobspace.co.nz/registration/?user_group_id=Employer">Register</a></li>
                                            <li><a href="https://www.myjobspace.co.nz/search-resumes/">Search CV</a></li>
                                            <li><a href="https://www.myjobspace.co.nz/pages/testimonials.php">Testimonials</a></li>
                                            <li><a href="https://www.myjobspace.co.nz/terms-of-use">Terms &amp; Conditions</a></li>
                                            <li><a href="https://www.myjobspace.co.nz/blog/category/4/">Employment cases of interest</a></li>                                  
                                        </ul>
                                    </li>
                                    <li className="navbar__item dropdown"><a title="Hunters" className="navbar__link dropdown-toggle" href="#"><i style={{fontSize:'20px',paddingRight:'5px'}} className="fa"></i>Job Hunters</a>
                                        <ul className="dropdown-menu">
                                            <li><a href="https://www.myjobspace.co.nz/registration/?user_group_id=JobSeeker">Register</a></li>
                                            <li><a href="https://www.myjobspace.co.nz/career/">Articles of interest</a></li>
                                            <li><a href="https://www.myjobspace.co.nz/blog">Blog</a></li>
                                            <li><a href="https://www.myjobspace.co.nz/find-jobs/">Advanced Job Search</a></li>                                  
                                        </ul>
                                    </li>
                                    <li className="navbar__item"><a title="Companies" className="navbar__link" href="https://www.myjobspace.co.nz/pages/testimonials.php">Testimonials</a></li>
                                    <li className="navbar__item"><a title="Contact Us" className="navbar__link" href="https://www.myjobspace.co.nz/contact/" style={{borderRight: 'none', paddingRight: '0px'}}>Contact Us</a></li>
                                </ul> 
                            </div>
                            <div className="top-popup">
                                <span>EMPLOYERS/JOB HUNTERS</span>
                            <div className="top-login"><img src="https://www.myjobspace.co.nz/templates/New/main/images/login-icon.png" /> <Link to="/login">Login</Link> | <Link to="/registration">Register</Link></div>
                            </div>
               <div className="right-contact">
                    <span className="top-chat"> <img src="https://www.myjobspace.co.nz/templates/New/main/images/chat-icon.png" /> <a href="#">Chat  with us</a></span>
                    <span className="top-contact"> <img src="https://www.myjobspace.co.nz/templates/New/main/images/phone-icon.png" /> <a href="tel:0800 486 329">0800 486 329</a></span>
            </div>  
            <div className="visible-md visible-lg">
                                    
   
<ul className="nav navbar-nav navbar-left">
    <li className="navbar__item"><a title="Jobs" className="navbar__link" href="https://www.myjobspace.co.nz">Home</a></li>
                <li className="navbar__item"><a title="Search a CV" className="navbar__link" href="https://www.myjobspace.co.nz/search-resumes/">Search</a></li>
        <li className="navbar__item"><Link to="/browse-by-company" title="Companies" className="navbar__link">Companies</Link></li>
    <li className="navbar__item dropdown"><a title="Employers" className="navbar__link dropdown-toggle" href="#"><i style={{fontSize:'20px', paddingRight: '5px'}} className="fa"></i>Employers</a>

         <ul className="dropdown-menu">
                <li><a href="https://www.myjobspace.co.nz/employer-products/
                                    ">Pricing</a></li>
                <li><a href="https://www.myjobspace.co.nz/registration/?user_group_id=Employer">Register</a></li>
                <li><a href="https://www.myjobspace.co.nz/search-resumes/">Search CV</a></li>
                <li><a href="https://www.myjobspace.co.nz/pages/testimonials.php">Testimonials</a></li>
                <li><a href="https://www.myjobspace.co.nz/terms-of-use">Terms &amp; Conditions</a></li>
                <li><a href="https://www.myjobspace.co.nz/blog/category/4/">Employment cases of interest</a></li>                                  
            </ul>

        </li>
        <li className="navbar__item dropdown"><a title="Hunters" className="navbar__link dropdown-toggle" href="#"><i style={{fontSize:'20px', paddingRight: '5px'}} className="fa"></i>Job Hunters</a>
         <ul className="dropdown-menu">
                
                <li><a href="https://www.myjobspace.co.nz/registration/?user_group_id=JobSeeker">Register</a></li>
                <li><a href="https://www.myjobspace.co.nz/career/">Articles of interest</a></li>
                <li><a href="https://www.myjobspace.co.nz/blog">Blog</a></li>
                <li><a href="https://www.myjobspace.co.nz/find-jobs/">Advanced Job Search</a></li>                                  
            </ul>

            </li>
            <li className="navbar__item"><a title="Companies" className="navbar__link" href="https://www.myjobspace.co.nz/pages/testimonials.php">Testimonials</a></li>
    
   
           
        
    <li className="navbar__item"><a title="Contact Us" className="navbar__link" href="https://www.myjobspace.co.nz/contact/" style={{borderRight: 'none', paddingRight: '0px'}}>Contact Us</a></li>
   
    </ul>

                
            </div>
        </div>
    </div>
</nav>
</div>
   )
    }
}
