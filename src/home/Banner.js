import React, {Component} from 'react';
import Popup from "reactjs-popup";
export default class Banner extends Component {
	constructor(props) {
    super(props);
    this.state = { open: false,list:[] };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  openModal() {
    this.setState({ open: true });
  }
  closeModal() {
    this.setState({ open: false });
  }
  componentDidMount(){
		fetch('/newapi/index.php/featuredjob/getoccupation/').then(res=>res.json()).then(json=>{
            this.setState({
                list:json,
            })
		});
		
	}

    render(){
    	var list = this.state.list;
        return (

            <div>

            	<div className="main-banner">
					<div className="quick-search__frontpage">
						<div className="container container-fluid quick-search">
							<div className="quick-search__wrapper well">
								<form id="quick-search-form" className="form-inline row">
								<fieldset className="first 2">
									<div className="form-group form-group__input">           
										<input type="text" className="searchText" name="keywords" id="keywords" placeholder="Keywords" />     
									</div>
									<div className="form-group form-group__input ">
										<div className="tree-input-field">
											<div className="left">
												<a href="#" id="tree-Occupations-options" onClick={this.openModal}>Categories</a>
												<div id="tree-Occupations-values" className="tree-values"></div>
											</div>
											<Popup
										          open={this.state.open}
										          closeOnDocumentClick
										          onClose={this.closeModal}
										        >
										          <div className="modal">
										            <a className="close" onClick={this.closeModal}>
										              &times;
										            </a>

										            <div className="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable ui-resizable">

										           <div className="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span className="ui-dialog-title" id="ui-dialog-title-messageBox">Occupations</span><a href="#" className="ui-dialog-titlebar-close ui-corner-all" role="button"><span className="ui-icon ui-icon-closethick">close</span></a></div>
										           </div>
										           <ul className="tree" id="tree-Occupations">
										            {list.map((val, index) => (
													   <li id="tree-li-716">
													      <div className="checkbox " level="0"><span className="strong">{val.caption}</span></div>
													      <input type="checkbox" id="tree-check-716" value="716" style={{display: "none"}} />
													      <ul id="tree-ul-716">
													      
													         <li id="tree-li-740">
													            <div className="checkbox " level="1"><span>fdgdsg</span></div>
													            <input type="checkbox" id="tree-check-740" value="740" style={{display: "none"}} />
													         </li>
													         
													      </ul>
													   </li>
													   ))}
													</ul>
										          </div>
										    </Popup>
											<div className="clr"></div>
										</div>
										<input type="hidden" name="Occupations[tree]" id="tree-Occupations-selected" value="" />                    
	                				</div>
									<div className="form-group form-group__input">
										<div className="tree-input-field">
											<div className="left">
												<a href="#" id="tree-city1-options" style={{display:"inline-block"}}>Location</a>
												<div id="tree-city1-values" className="tree-values"></div>
												<div id="tree-city1-values-more"></div>
												<div id="tree-city1-values-more-button" className="more-button" style={{display: "none",cursor: "pointer"}}>»&nbsp;more</div>
											</div>
											<div className="clr"></div>
										</div>
										<input type="hidden" name="city1[tree]" id="tree-city1-selected" />
									</div>
								</fieldset>
								<fieldset className="first 3">
									<div className="form-group form-group__input">
					

<span className="salary-abbr"><input type="text" name="Salary[monetary][not_less]" className="searchMoney"  /> <div className="to">to</div> <input type="text" name="Salary[monetary][not_more]" className="searchMoney" /></span>
<select name="Salary[monetary][currency]" id="Salary_list" style={{display: "none"}}>
	<option value="">Select Currency</option>
			<option value="NZD">$</option>
	</select>
				</div>
									<div className="form-group form-group__input">
										<div style={{minHeight: "26px"}}>
											<select multiple="multiple" style={{display: "none"}} id="EmploymentType" className="inputList 3 fieldTypeEmploymentType " name="EmploymentType[multi_like][]">
												<option value="76">Full time</option>
												<option value="77">Part time</option>
												<option value="78">Contractor</option>
												<option value="80">Seasonal</option>
												<option value="704">Casual</option>
												<option value="707">Temporary</option>
												<option value="722">Contract</option>
											</select>
											<button type="button" className="ui-multiselect ui-widget ui-state-default ui-corner-all multiCaption" title="" aria-haspopup="true" tabIndex="0"><span className="ui-icon ui-icon-triangle-2-n-s"></span><span>Employment Type</span></button>
										</div>	
									</div>
									<div className="form-group form-group__input">
			     <button type="submit" className="quick-search__find btn btn__orange btn__bold ">Find Jobs</button>
				</div>
								</fieldset>
							</form>
							</div>
						</div>
						</div>
					<div className="container text-center">
						<div className="main-banner__head">
						<h1>Search <span >11,504</span> live jobs</h1>
						    <div className="upload-text">
						            <h2>6,031
						</h2>
				            <p>UPLOADED CVS AVAILABLE</p>
				        </div>
					<p className="main-para">Finding your new job just got easier</p>
					<img src="https://www.myjobspace.co.nz/templates/New/main/images/under-line.png" />
					<div className="banner_launch">
						<div className="banner_launch_text">Myjobspace.co.nz, New Zealand’s largest Kiwi owned job company.</div>
						<span><img src="https://www.myjobspace.co.nz/templates/New/main/images/banner-innerlogo.png" /></span>
					</div>
				</div>

						</div>
					</div>
            </div>
            
        )
    }
}
