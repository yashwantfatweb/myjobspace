import React, {Component} from 'react';
import { Link } from 'react-router-dom';
export default class Blog extends Component {
	constructor (props){
        super(props);
        this.state={
            item:[],
            isLoaded:false,
        }
    }
    componentDidMount(){
        fetch('/newapi/index.php/banner/').then(res=>res.json()).then(json=>{
            this.setState({
                isLoaded:true,
                item:json,
            })
        });
    }
    render(){
    	var {isLoaded,item} = this.state;
        return (
            <div>

            	<section className="mini-banners">
					<div className="main-middle-banner-wrapper">
						{item.map((val, index) => (
						<div className="main-middle-banner" key={val.id}>
							<div className="picture-banner-wrapper first-banner ">
								<div className="picture-banner">
									<div className="text-banner">{val.title}</div>
										<img src={val.image_path} title={val.title} border="0" />
								</div>
							</div>
							<div className="read-more"><Link to={val.link} target="_blank">Read More</Link></div>
						</div>
						))}
					</div>
				</section>
            </div>
            
        )
    }
}
