import React, {Component} from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Link } from 'react-router-dom';
export default class Featured extends Component {
	constructor (){
		super();
		this.state = {
			list :[],
		};
	}


	componentDidMount(){
		fetch('/newapi/index.php/featuredjob/featuredcompany/').then(res=>res.json()).then(json=>{
            this.setState({
                list:json,
            })
        });
	}
    render(){
    	var list = this.state.list;
    	var settings = {
	      	slidesToShow: 3,
	    	slidesToScroll: 1,
	    	autoplay: true,
	    	autoplaySpeed: 2000,
	    };
        return (
            <div>
            	<section className="main-sections main-sections__featured-companies">
					<div id="featured-companies" className="container container-fluid text-center featured-companies">
						<h4 className="listing__title">Featured Companies</h4>
						<div className="jcarousel-skin-tango"><div className="jcarousel-container jcarousel-container-horizontal"><div className="jcarousel-clip jcarousel-clip-horizontal"><ul id="mycarousel" className="jcarousel-list jcarousel-list-horizontal">
							<Slider {...settings}>
							{list.map((val, index) => (
							<li key={val.sid} className="featured-company jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal" jcarouselindex="1">
								<Link to={val.url}>
									<div className="panel panel-default featured-company__panel">
										<div className="panel-body featured-company__panel-body">
											<img className="featured-company__image" src={val.saved_file_name} title={val.Title} />
										</div>
										<div className="panel-footer featured-company__panel-footer">
											<div className="featured-companies__name">
												<span>{val.Title}</span>
											</div>
											<div className="featured-companies__jobs">
												{val.job_count} job(s)
											</div>
										</div>
									</div>
								</Link>
							</li>
							))}
						</Slider>	
					</ul>
					
					</div>
					<div className="jcarousel-prev jcarousel-prev-horizontal"></div><div className="jcarousel-next jcarousel-next-horizontal"></div></div></div>
				</div>
				</section>
            </div>
            
        )
    }
}
