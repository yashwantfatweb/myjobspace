import React, {Component} from 'react';
import MetaTags from 'react-meta-tags';
import Banner from './Banner';
import FeaturedCompany from './Featured';
import FeaturedJob from './Featuredjob';
import HomeBlog from './Blog';
import HomeSignup from './HomeSignup';
//import AutoPlay from './AutoPlayCarousel';
export default class Home extends Component {
    render(){
        return (
            <div>
            	<MetaTags>
		            <title>Jobs in New Zealand - Job Search - Job Vacancies - MyJobSpace NZ</title>
		            <meta id="meta-description" name="description" content="Thousands of jobs in New Zealand. Proudly 100% kiwi owned. Our mission to develop the user-friendly job site for job hunters and employers. View our Job Vacancies." />
		            <meta id="og-title" property="og:title" content="MyApp" />
		            <meta id="og-image" property="og:image" content="path/to/image.jpg" />
		         </MetaTags>
                <Banner />
            	<FeaturedCompany />
                <FeaturedJob />
            	<HomeBlog />
            	<HomeSignup />
            </div>
            
        )
    }
}
