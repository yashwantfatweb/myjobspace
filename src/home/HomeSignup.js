import React, {Component} from 'react';
export default class HomeSignup extends Component {
    render(){
        return (
            <div>
            	<section className="main-sections main-sections__alert alert">
                    <div className="container container-fluid">
                        <div className="alert__block subscribe__description">
                            <h3>Sign up for job alerts</h3>
                            <div>Get job alerts straight to your inbox.</div>
                            <div>Enter your email to get started. You will be able to unsubscribe at any moment.</div>
                        </div>
                        <div className="alert__block alert__block-form">
                            <form id="create-alert" className="well alert__form">
                                
                                <div className="alert__messages">
                                </div>
                                <div className="form-group alert__form__input">
                                    <input type="email" className="form-control" name="email" placeholder="Your email" />
                                </div>
                                <div className="form-group alert__form__input">
                                    <select className="form-control" name="email_frequency">
                                        <option value="daily">Daily</option>
                                        <option value="weekly">Weekly</option>
                                        <option value="monthly">Monthly</option>
                                    </select>
                                </div>
                                <div className="form-group alert__form__input text-center">
                                    <input type="submit" name="save" value="Create alert" className="btn__submit-modal btn btn__orange btn__bold" />
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
            
        )
    }
}
