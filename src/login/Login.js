import React, {Component} from 'react';

export default class Login extends Component {
    render(){
        return (
            <div>
                <div id="main-div-after" class="innerdiv_forall">
					<h1>Sign In</h1>
					<form action="https://www.myjobspace.co.nz/login/" method="post" id="loginForm">
						<input type="hidden" name="return_url" value="" />
						<input type="hidden" name="action" value="login" />
						<fieldset>
							<div class="inputName">Username</div>
							<div class="inputField"><input type="text" name="username" /></div>
						</fieldset>
						<fieldset>
							<div class="inputName">Password</div>
							<div class="inputField"><input type="password" name="password" /></div>
						</fieldset>
						<fieldset>
							<div class="inputName">&nbsp;</div>
							<div class="inputField"><input type="checkbox" name="keep" id="keep" /><label for="keep"> Keep me signed in</label></div>
						</fieldset>
						<fieldset>
							<div class="inputName">&nbsp;</div>
							<div class="inputButton"><input type="submit" value="Login" class="button" /></div>
						</fieldset>
					</form>
					<br />
					<a href="/password-recovery">Forgot Your Password?</a>&nbsp;|&nbsp; <a href="registration">Registration</a>
					<div class="soc_reg_form"><div class="social_plugins_div">
					<span class="login_buttons_txt">
						Social network you want to login/join with:
					</span>
					<span class="social-buttons">
						<a href="https://www.myjobspace.co.nz/social/?network=facebook" class="social_login_button slb_facebook" title="Connect using Facebook"></a>
						<a href="https://www.myjobspace.co.nz/social/?network=google_plus" class="social_login_button slb_google_plus" title="Connect using Google+"></a>
						<a href="https://www.myjobspace.co.nz/social/?network=linkedin" class="social_login_button slb_linkedin" title="Connect using Linkedin"></a>
					</span>
					<div class="clr"></div>
				</div>
			</div>
			</div>
			</div>
			
            
        )
    }
}
